const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 5001;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.get("/result", (req, res) => res.send("MABUHAY!"));

app.listen(PORT, () => {
	console.log('Running on port ' + PORT);
})